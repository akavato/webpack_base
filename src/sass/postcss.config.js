module.exports = {
  plugins: [
    require('tailwindcss'),
    require('postcss-combine-media-query'),
    require('autoprefixer'),
    require('cssnano')({
      preset: ['default', {
        discardComments: {
          removeAll: true
        }
      }]
    })
  ],
  options: {
    ident: 'postcss'
  }
};
