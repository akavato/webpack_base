const fs = require('fs');
const path = require('path');
const svgToMiniDataURI = require('mini-svg-data-uri');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

function generateHtmlPlugins(templateDir) {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
  return templateFiles.map(item => {
    const parts = item.split('.');
    const name = parts[0];
    const extension = parts[1];
    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
      inject: true
    });
  });
}

module.exports = [{
  name: 'prod',
  mode: 'production',
  context: path.resolve(__dirname, 'src'),
  entry: {
    bundle: './js/common.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: './js/[name].js?v=[fullhash]',
    publicPath: ''
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          cacheDirectory: true
        }
      }]
    },
    {
      test: /\.html?$/,
      use: [{
        loader: 'html-loader',
        options: {
          minimize: false
        }
      }]
    },
    {
      test: /\.svg$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 2048,
          publicPath: './img/',
          outputPath: './img/',
          generator: (content) => svgToMiniDataURI(content.toString())
        }
      },
      {
        loader: 'image-webpack-loader',
        options: {
          disable: false,
          publicPath: './img/',
          outputPath: './img/',
          svgo: {
            enabled: true,
            plugins: [{
              cleanupAttrs: true
            },
            {
              collapseGroups: true
            },
            {
              convertColors: true
            },
            {
              convertShapeToPath: false
            },
            {
              convertTransform: true
            },
            {
              minifyStyles: true
            },
            {
              removeComments: true
            },
            {
              removeDesc: true
            },
            {
              removeMetadata: true
            },
            {
              removeUselessDefs: true
            },
            {
              removeEditorsNSData: true
            },
            {
              removeEmptyAttrs: true
            },
            {
              removeEmptyText: true
            },
            {
              removeEmptyContainers: true
            },
            {
              removeViewBox: false
            },
            {
              removeUnknownsAndDefaults: true
            },
            {
              removeRasterImages: true
            },
            {
              removeUnusedNS: true
            }
            ]
          }
        }
      }
      ]
    },
    {
      test: /\.(jpe?g|png|gif|ico)$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 2048,
          publicPath: './img/',
          outputPath: './img/'
        }
      },
      {
        loader: 'image-webpack-loader',
        options: {
          disable: false,
          publicPath: './img/',
          outputPath: './img/',
          mozjpeg: {
            progressive: true,
            quality: 65
          },
          optipng: {
            enabled: false
          },
          pngquant: {
            quality: [0.65, 0.90],
            speed: 2
          },
          gifsicle: {
            optimizationLevel: 3,
            interlaced: false
          }
          // webp: {
          //     quality: 75
          // }
        }
      }
      ]
    },
    {
      test: /\.(sa|sc|c)ss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        {
          loader: 'css-loader'
        },
        {
          loader: 'postcss-loader'
        },
        {
          loader: 'sass-loader'
        }
      ]
    },
    {
      test: /\.(woff2?|eot|ttf)$/,
      use: [{
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          publicPath: './'
        }
      }]
    }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    // new CopyWebpackPlugin([{
    //   from: './PHPMailer.php',
    //   to: './'
    // },
    // {
    //   from: './SMTP.php',
    //   to: './'
    // },
    // {
    //   from: './send.php',
    //   to: './'
    // },
    // {
    //   from: './robots.txt',
    //   to: './'
    // },
    // {
    //   from: 'img/favicon/*.png',
    //   to: './'
    // },
    // {
    //   from: 'img/favicon/*.svg',
    //   to: './'
    // },
    // {
    //   from: 'img/favicon/favicon.ico',
    //   to: './'
    // }
    // ]),
    new MiniCssExtractPlugin({
      filename: 'main.min.css?v=[fullhash]'
    })
  ].concat(generateHtmlPlugins('./src/html')),
  resolve: {
    modules: [
      path.resolve(path.join(__dirname, 'node_modules')),
      path.resolve(__dirname, './js')
    ]
  },
  externals: {
    // jquery: 'jQuery'
  },
  devtool: false,
  performance: {
    hints: 'warning',
    maxAssetSize: 200000,
    assetFilter: function(assetFilename) {
      return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
    }
  }
},
{
  name: 'dev',
  mode: 'development',
  context: path.resolve(__dirname, 'src'),
  entry: {
    bundle: './js/common.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: ''
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          cacheDirectory: true
        }
      }]
    },
    {
      test: /\.html?$/,
      use: [{
        loader: 'html-loader',
        options: {
          minimize: false
        }
      }]
    },
    {
      test: /\.(jpe?g|png|gif|svg|ico)$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 2048,
          name: '[path][name].[ext]',
          publicPath: './',
          outputPath: './'
        }
      }]
    },
    {
      test: /\.(sa|sc|c)ss$/,
      use: [{
        loader: MiniCssExtractPlugin.loader
      },
      {
        loader: 'css-loader',
        options: {
          sourceMap: true
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          sourceMap: true
        }
      },
      {
        loader: 'sass-loader',
        options: {
          sourceMap: true
        }
      }
      ]
    },
    {
      test: /\.(woff2?|eot|ttf)$/,
      use: [{
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          publicPath: '../'
        }
      }]
    }
    ]
  },
  plugins: [
    // new CopyWebpackPlugin([
    //     {
    //         from: './PHPMailer.php',
    //         to: './'
    //     },
    //     {
    //         from: './SMTP.php',
    //         to: './'
    //     },
    //     {
    //         from: './send.php',
    //         to: './'
    //     },
    //     {
    //         from: './robots.txt',
    //         to: './'
    //     },
    //     {
    //         from: 'img/**/*',
    //         to: './'
    //     }
    // ]),
    new StylelintPlugin({
      // cache: true,
      // failOnError: true
      fix: true
    }),
    new MiniCssExtractPlugin({
      filename: 'main.min.css'
    })
  ].concat(generateHtmlPlugins('./src/html')),
  resolve: {
    modules: [
      path.resolve(path.join(__dirname, 'node_modules')),
      path.resolve(__dirname, './js')
    ]
  },
  externals: {
    // jquery: 'jQuery'
  },
  devtool: 'source-map',
  performance: {
    // hints: 'warning',
    maxAssetSize: 200000,
    assetFilter: function(assetFilename) {
      return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
    }
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    watchContentBase: true,
    compress: true,
    port: 3000,
    historyApiFallback: {
      rewrites: [{
        from: /./,
        to: '/404.html'
      }]
    },
    stats: 'normal'
    // writeToDisk: true
  }
}
];
